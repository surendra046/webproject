package com.example.demo;


/*
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.springframework.boot.CommandLineRunner;*/
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Greeting;

@SpringBootApplication
public class DemoApplication {

	
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@Controller
	@RequestMapping("/hello-world")
	public class HelloWorldController {

	    private static final String template = "Hello, %s!";
	    private final AtomicLong counter = new AtomicLong();

	    @RequestMapping(method=RequestMethod.GET)
	    public @ResponseBody Greeting sayHello(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) {
	        return new Greeting(1, "Hello");
	    }

	}
}
/*	@Bean
	CommandLineRunner runner(ReservationRepository rr){
		return args->{
			Arrays.asList("Ra,Moham".split(",")).forEach(n->rr.save(new Reservation()));
			
			rr.findAll().forEach(System.out::println);
			rr.findReservationName("Moham").forEach(System.out::println);
		};
	}
}*/


/*
interface ReservationRepository extends JpaRepository<Reservation,Long>{
	Collection<Reservation> findReservationName(String name);
}


@Entity
class Reservation{
	@Id
	@GeneratedValue
	private Long id;
	
	private String reservationName;
	
	public Long getId() {
		return id;
	}
	
	public Reservation(){
		
	}
	
	public String getReservationName() {
		return reservationName;
	}

	
	

	public Reservation(String reservationName){
		this.reservationName=reservationName;
	}
	
	public String toString(){
		return "Reservation {"+reservationName +"reservationName "+id +"}";
	}
}*/